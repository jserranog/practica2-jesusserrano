package programa;

import java.util.Scanner;
import metodos.Metodos;

public class Programa {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int opcion;
		
		do {
			System.out.println("Programa de librerías");
			System.out.println("1 - Cuenta ascendente");
			System.out.println("2 - Imprimir mensaje");
			System.out.println("3 - Suma");
			System.out.println("4 - Mayusculas");
			System.out.println("5 - Salir");
			System.out.print("Introduce una opcion: ");
			
			opcion = input.nextInt();
			
			switch (opcion) {
			case 1:
				System.out.println("\n________________________\n");
				Metodos.cuentaAscendente();;
				break;
				
			case 2:
				System.out.println("\n________________________\n");
				Metodos.imprimirMensaje();
				break;
			case 3:
				System.out.println("\n________________________\n");
				System.out.print("Introduzca el primer sumando: ");
				int sumando1 = input.nextInt();
				System.out.println();
				System.out.print("Introduzca el segundo sumando: ");
				int sumando2 = input.nextInt();
				System.out.println();
				System.out.println(Metodos.suma(sumando1, sumando2));
				break;
			case 4:
				System.out.println("\n________________________\n");
				System.out.print("Introduzca una cadena: ");
				input.nextLine();
				String cadena = input.nextLine();
				System.out.println(Metodos.mayusculas(cadena));
				break;
			}
		} while(opcion != 5);
		
		input.close();
	}
}
