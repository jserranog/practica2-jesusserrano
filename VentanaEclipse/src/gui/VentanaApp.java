package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;
import javax.swing.JTabbedPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.ImageIcon;
import java.awt.Color;

public class VentanaApp extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaApp frame = new VentanaApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaApp() {
		setTitle("MusicApp");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 555, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		JMenuItem mntmAbrirArchivoDe = new JMenuItem("Abrir archivo de audio");
		mnMenu.add(mntmAbrirArchivoDe);
		
		JMenuItem mntmAbrirArchivoDe_1 = new JMenuItem("Abrir archivo de video");
		mnMenu.add(mntmAbrirArchivoDe_1);
		
		JMenu mnSocial = new JMenu("Social");
		mnMenu.add(mnSocial);
		
		JMenuItem mntmAmigos = new JMenuItem("Amigos");
		mnSocial.add(mntmAmigos);
		
		JMenuItem mntmComunidad = new JMenuItem("Comunidad");
		mnSocial.add(mntmComunidad);
		
		JMenuItem mntmForo = new JMenuItem("Foro");
		mnSocial.add(mntmForo);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnMenu.add(mntmSalir);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(112, 0, 315, 31);
		contentPane.add(toolBar);
		
		JButton btnAmigos = new JButton("Amigos");
		toolBar.add(btnAmigos);
		
		JButton btnComunidad = new JButton("Comunidad");
		btnComunidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		toolBar.add(btnComunidad);
		
		JButton btnForo = new JButton("Foro");
		toolBar.add(btnForo);
		
		JButton btnPaginaWeb = new JButton("Pagina Web");
		toolBar.add(btnPaginaWeb);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(22, 43, 503, 191);
		contentPane.add(tabbedPane);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(50, 205, 50));
		tabbedPane.addTab("Inicio", null, panel_3, null);
		panel_3.setLayout(null);
		
		JLabel lblHolaJess = new JLabel("Hola: Jesús");
		lblHolaJess.setBounds(399, 12, 87, 15);
		panel_3.add(lblHolaJess);
		
		JLabel lblSincronizar = new JLabel("Sincronizar:");
		lblSincronizar.setBounds(399, 39, 87, 15);
		panel_3.add(lblSincronizar);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Spotify", "Last.fm"}));
		comboBox.setBounds(374, 66, 112, 24);
		panel_3.add(comboBox);
		
		JButton btnSincronizar = new JButton("Sincronizar");
		btnSincronizar.setIcon(new ImageIcon(VentanaApp.class.getResource("/images/spotify-logo-png-open-2000.png")));
		btnSincronizar.setBounds(336, 102, 150, 50);
		panel_3.add(btnSincronizar);
		
		JLabel lblNewLabel = new JLabel("www.PaginaWeb.com");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 12));
		lblNewLabel.setBounds(93, 71, 165, 15);
		panel_3.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Crear Playlist", null, panel, null);
		panel.setLayout(null);
		
		JButton btnCrear = new JButton("Crear");
		btnCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnCrear.setBounds(193, 127, 117, 25);
		panel.add(btnCrear);
		
		textField = new JTextField();
		textField.setBounds(193, 73, 114, 19);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblNombreDeLa = new JLabel("Nombre de la Playlist:");
		lblNombreDeLa.setBounds(167, 35, 160, 15);
		panel.add(lblNombreDeLa);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Consultar Playlist", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblIntroduzcaElNombre = new JLabel("Introduzca el nombre de la Playlist a buscar");
		lblIntroduzcaElNombre.setBounds(12, 12, 321, 15);
		panel_1.add(lblIntroduzcaElNombre);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 39, 303, 97);
		panel_1.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		JButton btnBuscar_1 = new JButton("Buscar");
		btnBuscar_1.setBounds(353, 127, 117, 25);
		panel_1.add(btnBuscar_1);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1262300400000L), new Date(1262300400000L), new Date(1560290400000L), Calendar.DAY_OF_YEAR));
		spinner.setBounds(353, 38, 117, 20);
		panel_1.add(spinner);
		
		JLabel lblCreadaDesde = new JLabel("Creada desde:");
		lblCreadaDesde.setBounds(353, 12, 104, 15);
		panel_1.add(lblCreadaDesde);
		
		JLabel lblHasta = new JLabel("Hasta:");
		lblHasta.setBounds(353, 70, 70, 15);
		panel_1.add(lblHasta);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerDateModel(new Date(1560290400000L), new Date(1262300400000L), new Date(1560290400000L), Calendar.DAY_OF_YEAR));
		spinner_1.setBounds(353, 95, 117, 20);
		panel_1.add(spinner_1);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Importar Playlist", null, panel_2, null);
		panel_2.setLayout(null);
		
		JLabel lblIndiqueLaRuta = new JLabel("Indique la ruta de la playlist:");
		lblIndiqueLaRuta.setBounds(12, 12, 204, 15);
		panel_2.add(lblIndiqueLaRuta);
		
		textField_1 = new JTextField();
		textField_1.setBounds(12, 32, 204, 19);
		panel_2.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnExplorar = new JButton("Explorar...");
		btnExplorar.setBounds(246, 29, 117, 25);
		panel_2.add(btnExplorar);
		
		JLabel lblIndiqueElNombre = new JLabel("Indique el nombre de usuario del que quiere buscar playlists:");
		lblIndiqueElNombre.setFont(new Font("Dialog", Font.BOLD, 10));
		lblIndiqueElNombre.setBounds(12, 63, 351, 15);
		panel_2.add(lblIndiqueElNombre);
		
		textField_2 = new JTextField();
		textField_2.setBounds(12, 90, 204, 19);
		panel_2.add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(246, 90, 117, 25);
		panel_2.add(btnBuscar);
		
		JRadioButton rdbtnLocal = new JRadioButton("Local");
		buttonGroup.add(rdbtnLocal);
		rdbtnLocal.setBounds(12, 133, 149, 23);
		panel_2.add(rdbtnLocal);
		
		JRadioButton rdbtnExterna = new JRadioButton("Externa");
		buttonGroup.add(rdbtnExterna);
		rdbtnExterna.setBounds(214, 133, 149, 23);
		panel_2.add(rdbtnExterna);
	}
}
